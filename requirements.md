# App Requirements
+ Home screen
  - daily tip, non-intrusive
  - achievement progress
  - baby status
  - friend activity
+ Main menu
  - Activities
  - My Baby
  - Statistics
  - Community
+ Activities list
  - Walking, Yoga, Jogging, ...
  - Information
  - Icons for semesters
  - Find friends
  - Add workout time
+ My baby
  - items
    * food
    * clothing
    * toys
  - hunger level
    * food decreases hunger
    * drops every completed goal segment
  - clothing & toys 
    * chance to drop on completion of weekly goal
+ Statistics
  - blood pressure
  - weight 
  - average hearth rate
+ Community
  - chatrooms
    * public and private
    * rooms for hospital departments
    * private room with gynecologist
  - find other women for activities
  - add friends / friend activity
+ Notifications
  - baby is hungry
  - weekly goals status (goal not completed at end of week)
+ Extra 
  - achievements (run 5 times in one week, complete weekly goal in a day, ...)
