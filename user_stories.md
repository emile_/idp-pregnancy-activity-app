# Olivia
Olivia is twee maanden zwanger en krijgt te horen dat haar bloeddruk te hoog ligt. Van de dokter moet ze meer gaan bewegen.

Olivia installeert de Pregnancy Activity App. Wanneer ze een gebruikersnaam en wachtwoord heeft gekozen kan ze haar baby avatar aanmaken met een aantal zelfgekozen kenmerken zoals oog, haar en huidskleur.

*Een paar weken later ...*

Op het startscherm ziet Olivia haar huidige voortgang ten opzichte van haar wekelijkse goal.

Olivia ziet dat Ava net haar derde weekelijkse goal behaald heeft, en dat zij deze week nog heel wat moet inhalen. 

Ook ziet ze dat Emma net is gaan zwemmen. Tot nu toe heeft ze dit nog niet durven doen omdat ze denkt dat dit slecht is voor de baby.

Dankzij de activiteit van Emma is ze gemotiveerd en gaat naar de activiteitenlijst om zichzelf te informeren over zwemmen tijdens zwangerschap.

Ze ziet dat zwemmen niet gevaarlijk is en een goede activiteit voor zwangere vrouwen omvat. Ze beslist om te zoeken naar andere vrouwen die samen willen zwemmen.

Na het zwemmen checked ze haar baby. Tijdens het zwemmen heeft ze een rammelaar gekregen als reward. Deze voegt ze nu toe aan haar baby.

Daarna kijkt ze even bij de statistieken om voortgang op vlak van gewicht te bepalen.


