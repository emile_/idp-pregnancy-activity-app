package com.example.pregnancyactivity.ui.baby.ui.baby

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.pregnancyactivity.R

class BabyViewFragment: Fragment() {

    companion object {
            fun newInstance() = BabyViewFragment()
        }
        private fun setBabyViewByType(babyItem: BabyItemType,
                                      babyHat: ImageView,
                                      babyTshirt: ImageView,
                                      babyPants: ImageView,
                                      babyHandToy: ImageView,
                                      babyBackground: ImageView
        ){
            when (babyItem){
                BabyItemType.BLUE_TSHIRT -> {
                    babyTshirt.setImageResource(R.drawable.tblue)
                    BabyItemContent.current_tshirt = babyItem
                }
                BabyItemType.RED_TSHIRT -> {
                    babyTshirt.setImageResource(R.drawable.tred)
                    BabyItemContent.current_tshirt = babyItem
                }
                BabyItemType.PURPLE_TSHIRT -> {
                    babyTshirt.setImageResource(R.drawable.tpurple)
                    BabyItemContent.current_tshirt = babyItem
                }
                BabyItemType.GREEN_PANTS -> {
                    babyPants.setImageResource(R.drawable.pgreen)
                    BabyItemContent.current_pants = babyItem
                }
                BabyItemType.YELLOW_PANTS -> {
                    babyPants.setImageResource(R.drawable.pyellow)
                    BabyItemContent.current_pants = babyItem
                }
                BabyItemType.CYAN_PANTS -> {
                    babyPants.setImageResource(R.drawable.pcyan)
                    BabyItemContent.current_pants = babyItem
                }
                BabyItemType.GREY_CAP -> {
                    babyHat.setImageResource(R.drawable.hgrey)
                    BabyItemContent.current_hat = babyItem
                }
                BabyItemType.ORANGE_CAP -> {
                    babyHat.setImageResource(R.drawable.horange)
                    BabyItemContent.current_hat = babyItem
                }
                BabyItemType.RATTLE -> {
                    babyHandToy.setImageResource(R.drawable.rattle)
                    BabyItemContent.current_hand_toy = babyItem
                }
                BabyItemType.SPOON -> {
                    babyHandToy.setImageResource(R.drawable.spoon)
                    BabyItemContent.current_hand_toy = babyItem
                }
                BabyItemType.TEDDY -> {
                    babyHandToy.setImageResource(R.drawable.teddy2)
                    BabyItemContent.current_hand_toy = babyItem
                }
                BabyItemType.MILK -> {
                }
                BabyItemType.NURSERY -> {
                    babyBackground.setImageResource(R.drawable.nursery)
                    BabyItemContent.current_background = babyItem
                }
                BabyItemType.PLAYGROUND -> {
                    babyBackground.setImageResource(R.drawable.playground)
                    BabyItemContent.current_background = babyItem
                }
                BabyItemType.FOREST -> {
                    babyBackground.setImageResource(R.drawable.forest)
                    BabyItemContent.current_background = babyItem
                }
            }
        }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        val view = inflater.inflate(R.layout.baby_view_layout, container, false)

        val babyHat: ImageView = view.findViewById(R.id.babyHatView)
        val babyTshirt: ImageView = view.findViewById(R.id.babyTshirtView)
        val babyPants: ImageView = view.findViewById(R.id.babyPantsView)
        val babyHandToy: ImageView = view.findViewById(R.id.babyHandToyView)
        val babyBackground: ImageView = view.findViewById(R.id.babyBackgroundView)

        setBabyViewByType(BabyItemContent.current_background, babyHat, babyTshirt, babyPants, babyHandToy, babyBackground)
        setBabyViewByType(BabyItemContent.current_hat, babyHat, babyTshirt, babyPants, babyHandToy, babyBackground)
        setBabyViewByType(BabyItemContent.current_tshirt, babyHat, babyTshirt, babyPants, babyHandToy, babyBackground)
        setBabyViewByType(BabyItemContent.current_pants, babyHat, babyTshirt, babyPants, babyHandToy, babyBackground)
        setBabyViewByType(BabyItemContent.current_hand_toy, babyHat, babyTshirt, babyPants, babyHandToy, babyBackground)

        return view

    }
}