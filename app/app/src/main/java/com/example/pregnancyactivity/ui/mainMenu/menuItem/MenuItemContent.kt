package com.example.pregnancyactivity.ui.mainMenu.menuItem

import android.media.Image
import com.example.pregnancyactivity.R
import com.example.pregnancyactivity.ui.activities.ActivitiesActivity
import com.example.pregnancyactivity.ui.baby.BabyActivity
import com.example.pregnancyactivity.ui.chat.ChatActivity
import com.example.pregnancyactivity.ui.statistics.StatisticsActivity
import java.util.ArrayList
import java.util.HashMap

object MenuItemContent {

    val ITEMS: MutableList<MenuItem> = ArrayList()
    val ITEM_MAP: MutableMap<String, MenuItem> = HashMap()

    init {
        addItem(createMenuItem("My baby", R.drawable.empty_baby, BabyActivity::class.java))
        addItem(createMenuItem("Activities", R.drawable.activities2, ActivitiesActivity::class.java))
        addItem(createMenuItem("Chat", R.drawable.chat, ChatActivity::class.java))
        addItem(createMenuItem("Statistics", R.drawable.stats, StatisticsActivity::class.java))
    }

    private fun addItem(item: MenuItem) {
        MenuItemContent.ITEMS.add(item)
        ITEM_MAP.put(item.name, item)
    }

    private fun createMenuItem(name: String, imageResource: Int, nextActivity: Class<*>): MenuItem {
        return MenuItem(name, imageResource, nextActivity)
    }

    data class MenuItem(val name: String, val imageResource: Int, val nextActivity: Class<*>) {
        override fun toString(): String = name
    }

}