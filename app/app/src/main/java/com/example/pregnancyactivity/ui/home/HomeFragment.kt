package com.example.pregnancyactivity.ui.home

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.transition.Slide
import android.util.Log
import android.util.TypedValue
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.pregnancyactivity.R
import com.example.pregnancyactivity.ui.baby.BabyActivity
import java.time.LocalDate
import java.time.ZoneId
import java.util.*

class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        homeViewModel =
                ViewModelProvider(this).get(HomeViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_home, container, false)
        val textView: TextView = root.findViewById(R.id.text_home)
        homeViewModel.text.observe(viewLifecycleOwner, Observer {
            textView.text = it
        })


        //shared Preferences achievements
        val sharedPreferenceAchievements = context?.getSharedPreferences("PREFERENCES_ACHIEVEMENTS", Context.MODE_PRIVATE)
        val editorAchievements = sharedPreferenceAchievements?.edit()

        //shared preferences algemene data
        val sharedPreferenceGeneral = context?.getSharedPreferences("PREFERENCES_GENERAL", Context.MODE_PRIVATE)
        val currentDay = Date()
        val currenttime: Long = currentDay.time
        var savedDate: Long = sharedPreferenceGeneral!!.getLong("start_day", 0.toLong())
        val editorGeneral = sharedPreferenceGeneral.edit()
        if (savedDate == 0.toLong()) { // starday not yet set
            editorGeneral.putLong("start_day", currenttime)
            editorGeneral.apply()
            savedDate = currenttime
        }
        val diff: Long = currenttime - savedDate
        val seconds = diff / 1000
        val minutes = seconds / 60
        val hours = minutes / 60
        var days = hours / 24
        days += 1
        val weeks = days/7

        if (editorAchievements != null) {
            if (days > 50) {
                editorAchievements.putInt("achievement4", 50)
            } else {
                editorAchievements.putInt("achievement4", days.toInt())
            }
            editorAchievements.apply()
        }


        //Log.d("DATE TEST", currenttime.toString())

        // preferences daily AP
        val dailyGoal : Float = sharedPreferenceGeneral.getInt("daily_goal", 30).toFloat()

        var previousDay = sharedPreferenceGeneral.getLong("previous_day", 0.toLong())
        if (previousDay == 0.toLong()) {
            previousDay = currenttime
            editorGeneral.putLong("previous_day", previousDay)
            editorGeneral.apply()
        }
        val prevDate = Date(previousDay)
        val prev = prevDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate()
        val current = currentDay.toInstant().atZone(ZoneId.systemDefault()).toLocalDate()


        if (prev.toEpochDay() < current.toEpochDay()) { // new day so reset daily goal and check weekly
            val dailyAP = sharedPreferenceGeneral.getInt("daily_AP", 0)
            if (dailyAP >= dailyGoal) {
                var weeklyCompleted = sharedPreferenceGeneral.getInt("weekly_completed", 0)
                if (weeklyCompleted < 5) {
                    weeklyCompleted += 1
                }
                editorGeneral.putInt("weekly_completed", weeklyCompleted)
            }
            editorGeneral.putInt("daily_AP", 0)
            editorGeneral.putLong("previous_day", currenttime)
            editorGeneral.commit()
        }
        val dailyAP = sharedPreferenceGeneral.getInt("daily_AP", 0)

        // preferneces weekly goals
        var savedWeek = sharedPreferenceGeneral.getInt("saved_week", 0)
        val currentWeek = weeks.toInt()
        if (savedWeek < currentWeek){ // last saved week number lower than this week number -> reset week
            editorGeneral.putInt("saved_week", currentWeek)
            editorGeneral.putInt("weekly_completed", 0)
            editorGeneral.commit()
        }

        val weeklyCompleted = sharedPreferenceGeneral.getInt("weekly_completed", 0)





        val weeklyGroen : TextView = root.findViewById(R.id.weekly_groene_balk)
        setWeekly(weeklyCompleted, weeklyGroen)


        var procent : Float = (dailyAP.toFloat()/dailyGoal)
        if (procent > 1f) {
            procent = 1f
        }
        val pixels : Int = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, procent * 300f, resources.displayMetrics).toInt()

        val groeneBalk = root.findViewById<TextView>(R.id.daily_groene_balk)
        val param: ViewGroup.LayoutParams? = groeneBalk.layoutParams
        if (param != null) {
            param.width = pixels
            groeneBalk.layoutParams = param
        }

        val temp : String = "$dailyAP/${dailyGoal.toInt()} AP"
        root.findViewById<TextView>(R.id.daily_procent).text = temp
//        val temp2 : String = "Daily goal : $dailyAP/30 AP"
//        root.findViewById<TextView>(R.id.dialy_text).text = temp2



//        weeklyGoalContainer.setOnClickListener{
//            val popupWindow: PopupWindow = initPopUp(inflater, dailyAP)
//            popupWindow.showAtLocation(this.view, Gravity.CENTER, 0, 0);
//        }


        val babyView: View = root.findViewById(R.id.babyViewFragment)
        babyView.setOnClickListener{
            val rActivity = requireActivity()
            val intent = Intent(rActivity, BabyActivity::class.java)
            rActivity.startActivity(intent)
        }

        val settingsBtn = root.findViewById<Button>(R.id.btn_settings_daily)
        settingsBtn.setOnClickListener {
            val popupWindow: PopupWindow = initPopUp(inflater)
            popupWindow.showAtLocation(this.view, Gravity.CENTER, 0, 0);
        }


        return root
    }


    fun setWeekly(i: Int, progress : TextView) {
        val pixels : Int = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, i * 60f, resources.displayMetrics).toInt()
        //Log.d("WEEKLY DEBUG", pixels.toString())

        val param: ViewGroup.LayoutParams? = progress.layoutParams
        if (param != null) {
            param.width = pixels
            progress.layoutParams = param
        }

    }

    private fun initPopUp(inflater: LayoutInflater) : PopupWindow {
        // Inflate a custom view using layout inflater
        val popUpView = inflater.inflate(R.layout.popup_home_settings,null)

        // Initialize a new instance of popup window
        val popupWindow = PopupWindow(
                popUpView, // Custom view to show in popup window
                LinearLayout.LayoutParams.MATCH_PARENT, // Width of popup window  wrap: LinearLayout.LayoutParams.WRAP_CONTENT
                300, // Window height
                true    // nodig zodat ge er langs kunt klikken zodat het weg gaat
        )

        // Set an elevation for the popup window
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            popupWindow.elevation = 10.0F
        }
        // If API level 23 or higher then execute the code
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            // Create a new slide animation for popup window enter transition
            val slideIn = Slide()
            slideIn.slideEdge = Gravity.LEFT
            popupWindow.enterTransition = slideIn

            // Slide animation for popup window exit transition
            val slideOut = Slide()
            slideOut.slideEdge = Gravity.RIGHT
            popupWindow.exitTransition = slideOut

        }

        // dismiss the popup window when touched
        popUpView.setOnTouchListener { v, event ->
            popupWindow.dismiss()
            true
        }
        popUpView.findViewById<ImageButton>(R.id.ConfirmDailyButton).setOnClickListener{

            val edit_text = popUpView.findViewById<EditText>(R.id.editTextDailyGoal)
            val newGoal : Int = edit_text.text.toString().toInt()
            //shared preferences algemene data
            val sharedPreferenceGeneral = requireContext().getSharedPreferences("PREFERENCES_GENERAL", Context.MODE_PRIVATE)
            val editorGeneral = sharedPreferenceGeneral.edit()



            editorGeneral.putInt("daily_goal", newGoal)

            editorGeneral.apply()

            Log.d("HOME", "Daily goal changed : $newGoal")
            popupWindow.dismiss()
            true
        }

        return popupWindow
    }
}