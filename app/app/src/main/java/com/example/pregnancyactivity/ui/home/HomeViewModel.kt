package com.example.pregnancyactivity.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class HomeViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "Daily tip: Take a prenatal vitamin."
    }
    val text: LiveData<String> = _text
}