package com.example.pregnancyactivity.ui.baby.ui.baby

enum class BabyItemType {
    NONE, BLUE_TSHIRT, RED_TSHIRT, PURPLE_TSHIRT, GREEN_PANTS, YELLOW_PANTS, CYAN_PANTS, GREY_CAP, ORANGE_CAP,
    RATTLE, SPOON, TEDDY,
    MILK,
    FOREST, PLAYGROUND, NURSERY

}