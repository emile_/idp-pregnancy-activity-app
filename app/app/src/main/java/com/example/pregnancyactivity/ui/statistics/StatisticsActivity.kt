package com.example.pregnancyactivity.ui.statistics

import android.content.Context
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.transition.Slide
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.example.pregnancyactivity.R
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.jjoe64.graphview.DefaultLabelFormatter
import com.jjoe64.graphview.GraphView
import com.jjoe64.graphview.series.DataPoint
import com.jjoe64.graphview.series.LineGraphSeries
import org.w3c.dom.Text
import java.text.SimpleDateFormat
import java.util.*

class StatisticsActivity : AppCompatActivity() {
    @ExperimentalStdlibApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_statistics)

        // calling the action bar
        var actionBar = getSupportActionBar()

        // showing the back button in action bar
        if (actionBar != null) {
            actionBar.title = "Statistics"
            actionBar.setDisplayHomeAsUpEnabled(true)
        }


        // shared preferences algemene data
        val sharedPreferenceGeneral = getSharedPreferences("PREFERENCES_GENERAL", Context.MODE_PRIVATE)
        val editorGeneral = sharedPreferenceGeneral.edit()

        // general data
        val stats_textview_1 : TextView = findViewById(R.id.stats_TextView_1)
        val stats_textview_2 : TextView = findViewById(R.id.stats_TextView_2)
        val stats_textview_3 : TextView = findViewById(R.id.stats_TextView_3)

        // weeks pregnant
        val currentDay = Date()
        val currenttime: Long = currentDay.time
        var savedDate: Long = sharedPreferenceGeneral!!.getLong("start_day", 0.toLong())
        val diff: Long = currenttime - savedDate
        val seconds = diff / 1000
        val minutes = seconds / 60
        val hours = minutes / 60
        var days = hours / 24
        days += 1
        val weeks = ((days/7) + 1).toInt()

        var tempString : String = "Weeks pregnant : $weeks weeks"
        stats_textview_1.text = tempString

        // pragancy phase
        tempString = "Pregnancy phase : phase 1"
        if (weeks > 1) {
            tempString = "Pregnancy phase : phase 2"
        }
        if (weeks > 10) {
            tempString = "Pregnancy phase : phase 3"
        }
        stats_textview_2.text = tempString

        // total AP collected
        val totalAP = sharedPreferenceGeneral.getInt("total_AP", 0)
        tempString = "Total AP collected : $totalAP"
        stats_textview_3.text = tempString





        // weight graph

        var graph: GraphView = findViewById(R.id.graph_weight)


        // generate Dates
        val calendar: Calendar = Calendar.getInstance()
        val d1: Date = calendar.getTime()
        calendar.add(Calendar.DATE, 1)
        val d2: Date = calendar.getTime()
        calendar.add(Calendar.DATE, 1)
        val d3: Date = calendar.getTime()
        calendar.add(Calendar.DATE, 3)
        val d4: Date = calendar.getTime()
        calendar.add(Calendar.DATE, 1)
        val d5: Date = calendar.getTime()


        var weightList = sharedPreferenceGeneral.getString("weight_list", "")
        var list : MutableList<String> = weightList!!.split(";").toMutableList()
        list.removeLast()

        var dataArray  = mutableListOf<DataPoint>()
        if (list != null) {
            for (element in list) {
                val splitted = element.split(",")
                val date = Date(splitted[0])
                val value = splitted[1].toDouble()
                dataArray.add(DataPoint(date, value))
            }
        }

        val dataPrediction = (arrayOf<DataPoint>(DataPoint(d1, 80.0), DataPoint(d2, 82.0), DataPoint(d3, 82.5), DataPoint(d4, 83.0), DataPoint(d5, 83.0) ))
        var seriesPrediction = LineGraphSeries<DataPoint>(dataPrediction)
        // styling series
        seriesPrediction.setTitle("Ideal Weight")
        seriesPrediction.setColor(Color.RED)
        seriesPrediction.setDrawDataPoints(true)
        seriesPrediction.setDataPointsRadius(10f)
        seriesPrediction.setThickness(8);
        graph.addSeries(seriesPrediction)

        var series = LineGraphSeries<DataPoint>(dataArray.toTypedArray())
        graph.addSeries(series)
        // activate horizontal zooming and scrolling
        graph.viewport.isScalable = true
        graph.viewport.isScrollable = true
        //graph.gridLabelRenderer.numHorizontalLabels = 10

        // styling series
        series.setTitle("Weight")
        series.setColor(Color.GREEN)
        series.setDrawDataPoints(true)
        series.setDataPointsRadius(10f)
        series.setThickness(8);

        //set data label formatter
        //graph.gridLabelRenderer.labelFormatter = DateAsXAxisLabelFormatter(this)
        graph.gridLabelRenderer.labelFormatter = object:DefaultLabelFormatter() {
            override fun formatLabel(value:Double, isValueX:Boolean):String {
                if (isValueX)
                {
                    // show normal x values
                    //val date = Date(value)
                    val l: Long = value.toLong()
                    var date = Date(l)

                    //val pattern = "dd/\nMM/\nyy"
                    val pattern = "dd/MM"
                    val simpleDateFormat = SimpleDateFormat(pattern)
                    val text: String = simpleDateFormat.format(date)


                    return "$text"
                    //return super.formatLabel(value, isValueX)
                }
                else
                {
                    // show currency for y values
                    return super.formatLabel(value, isValueX)
                }
            }
        }
        graph.gridLabelRenderer.numHorizontalLabels = dataArray.size + 1

        // set manual x bounds to have nice steps
        graph.viewport.setMinX(d1.time.toDouble())
        graph.viewport.setMaxX(d4.time.toDouble())
        graph.viewport.isXAxisBoundsManual = true

        // as we use dates as labels, the human rounding to nice readable numbers
        // is not necessary
        graph.gridLabelRenderer.setHumanRounding(false)

        graph.gridLabelRenderer.verticalAxisTitle = "kg"
        graph.gridLabelRenderer.horizontalAxisTitle = "date"

        // set button on click
        findViewById<Button>(R.id.add_weight).setOnClickListener {
            val popupWindow: PopupWindow = initPopUp(layoutInflater)
            popupWindow.showAtLocation(findViewById(R.id.stats_container), Gravity.CENTER, 0, 0);
        }

        // display bp
        val bpText = findViewById<TextView>(R.id.stats_TextView_bp)
        val bp = sharedPreferenceGeneral.getString("bp_list", "No blood pressure recorded yet.")
        bpText.text = bp

        // button to add bp
        findViewById<Button>(R.id.add_bp).setOnClickListener {
            val popupWindow: PopupWindow = initPopUpBp(layoutInflater)
            popupWindow.showAtLocation(findViewById(R.id.stats_container), Gravity.CENTER, 0, 0);
        }

        // show activities feed
        val stats_TextView_activities : TextView = findViewById(R.id.stats_TextView_activities)
        val activitiesString = sharedPreferenceGeneral.getString("workouts", "No workouts entered")
        stats_TextView_activities.text = activitiesString

    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }


    private fun initPopUp(inflater: LayoutInflater) : PopupWindow {
        // Inflate a custom view using layout inflater
        val popUpView = inflater.inflate(R.layout.add_weight_popup,null)

        // Initialize a new instance of popup window
        val popupWindow = PopupWindow(
                popUpView, // Custom view to show in popup window
                LinearLayout.LayoutParams.MATCH_PARENT, // Width of popup window  wrap: LinearLayout.LayoutParams.WRAP_CONTENT
                300, // Window height
                true    // nodig zodat ge er langs kunt klikken zodat het weg gaat
        )

        // Set an elevation for the popup window
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            popupWindow.elevation = 10.0F
        }
        // If API level 23 or higher then execute the code
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            // Create a new slide animation for popup window enter transition
            val slideIn = Slide()
            slideIn.slideEdge = Gravity.LEFT
            popupWindow.enterTransition = slideIn

            // Slide animation for popup window exit transition
            val slideOut = Slide()
            slideOut.slideEdge = Gravity.RIGHT
            popupWindow.exitTransition = slideOut

        }

        // dismiss the popup window when touched
        popUpView.setOnTouchListener { v, event ->
            popupWindow.dismiss()
            true
        }
        popUpView.findViewById<ImageButton>(R.id.ConfirmWeightButton).setOnClickListener{

            val edit_text = popUpView.findViewById<EditText>(R.id.editTextWeight)
            val weight : Float = edit_text.text.toString().toFloat()
            //shared preferences algemene data
            val sharedPreferenceGeneral = getSharedPreferences("PREFERENCES_GENERAL", Context.MODE_PRIVATE)
            val editorGeneral = sharedPreferenceGeneral.edit()

            var weightList = sharedPreferenceGeneral.getString("weight_list", "")
            // generate Dates
            val calendar: Calendar = Calendar.getInstance()
            val currentDay: Date = calendar.getTime()
            weightList += "$currentDay,$weight;"
            editorGeneral.putString("weight_list", weightList)

            editorGeneral.apply()

            Log.d("STATS", "weight added : $currentDay, $weight")
            popupWindow.dismiss()
            true
        }

        return popupWindow
    }

    private fun initPopUpBp(inflater: LayoutInflater) : PopupWindow {
        // Inflate a custom view using layout inflater
        val popUpView = inflater.inflate(R.layout.add_bp_popup,null)

        // Initialize a new instance of popup window
        val popupWindow = PopupWindow(
            popUpView, // Custom view to show in popup window
            LinearLayout.LayoutParams.MATCH_PARENT, // Width of popup window  wrap: LinearLayout.LayoutParams.WRAP_CONTENT
            300, // Window height
            true    // nodig zodat ge er langs kunt klikken zodat het weg gaat
        )

        // Set an elevation for the popup window
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            popupWindow.elevation = 10.0F
        }


        // If API level 23 or higher then execute the code
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            // Create a new slide animation for popup window enter transition
            val slideIn = Slide()
            slideIn.slideEdge = Gravity.LEFT
            popupWindow.enterTransition = slideIn

            // Slide animation for popup window exit transition
            val slideOut = Slide()
            slideOut.slideEdge = Gravity.RIGHT
            popupWindow.exitTransition = slideOut

        }

        // dismiss the popup window when touched
        popUpView.setOnTouchListener { v, event ->
            popupWindow.dismiss()
            true
        }
        popUpView.findViewById<ImageButton>(R.id.ConfirmBpButton).setOnClickListener{

            val edit_text_upper = popUpView.findViewById<EditText>(R.id.editTextUpper)
            val edit_text_lower = popUpView.findViewById<EditText>(R.id.editTextLower)
            val upper : Int = edit_text_upper.text.toString().toInt()
            val lower : Int = edit_text_lower.text.toString().toInt()

            //shared preferences algemene data
            val sharedPreferenceGeneral = getSharedPreferences("PREFERENCES_GENERAL", Context.MODE_PRIVATE)
            val editorGeneral = sharedPreferenceGeneral.edit()

            var bpList = sharedPreferenceGeneral.getString("bp_list", "")
            // generate Dates
            val calendar: Calendar = Calendar.getInstance()
            val currentDay: Date = calendar.getTime()

            val pattern = "dd/MM/yy"
            val simpleDateFormat = SimpleDateFormat(pattern)
            val text: String = simpleDateFormat.format(currentDay)


            bpList += "$text: systolic: $upper, diastolic: $lower\n"
            editorGeneral.putString("bp_list", bpList)

            editorGeneral.apply()

            Log.d("STATS", "bp added : $upper, $lower")
            popupWindow.dismiss()

            val bpText = findViewById<TextView>(R.id.stats_TextView_bp)
            bpText.text = bpList

            true
        }

        return popupWindow
    }
}
