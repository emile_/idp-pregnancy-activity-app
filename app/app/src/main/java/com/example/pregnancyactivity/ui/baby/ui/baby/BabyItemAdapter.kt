package com.example.pregnancyactivity.ui.baby.ui.baby

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.example.pregnancyactivity.R

class BabyItemAdapter (
        private val values: List<BabyItemContent.BabyItem>,
        private val hungerBar: ProgressBar,
        private val babyHat: ImageView,
        private val babyTshirt: ImageView,
        private val babyPants: ImageView,
        private val babyHandToy: ImageView,
        private val babyBackground: ImageView

        ): RecyclerView.Adapter<BabyItemAdapter.ViewHolder>(){

        private fun setBabyViewByType(babyItem: BabyItemType){
                when (babyItem){
                        BabyItemType.MILK -> {
                                BabyItemContent.current_hunger += 15
                                hungerBar.progress = BabyItemContent.current_hunger
                        }
                        BabyItemType.BLUE_TSHIRT -> {
                                babyTshirt.setImageResource(R.drawable.tblue)
                                BabyItemContent.current_tshirt = babyItem
                        }
                        BabyItemType.RED_TSHIRT -> {
                                babyTshirt.setImageResource(R.drawable.tred)
                                BabyItemContent.current_tshirt = babyItem
                        }
                        BabyItemType.PURPLE_TSHIRT -> {
                                babyTshirt.setImageResource(R.drawable.tpurple)
                                BabyItemContent.current_tshirt = babyItem
                        }
                        BabyItemType.GREEN_PANTS -> {
                                babyPants.setImageResource(R.drawable.pgreen)
                                BabyItemContent.current_pants = babyItem
                        }
                        BabyItemType.YELLOW_PANTS -> {
                                babyPants.setImageResource(R.drawable.pyellow)
                                BabyItemContent.current_pants = babyItem
                        }
                        BabyItemType.CYAN_PANTS -> {
                                babyPants.setImageResource(R.drawable.pcyan)
                                BabyItemContent.current_pants = babyItem
                        }
                        BabyItemType.GREY_CAP -> {
                                babyHat.setImageResource(R.drawable.hgrey)
                                BabyItemContent.current_hat = babyItem
                        }
                        BabyItemType.ORANGE_CAP -> {
                                babyHat.setImageResource(R.drawable.horange)
                                BabyItemContent.current_hat = babyItem
                        }
                        BabyItemType.RATTLE -> {
                                babyHandToy.setImageResource(R.drawable.rattle)
                                BabyItemContent.current_hand_toy = babyItem
                        }
                        BabyItemType.SPOON -> {
                                babyHandToy.setImageResource(R.drawable.spoon)
                                BabyItemContent.current_hand_toy = babyItem
                        }
                        BabyItemType.TEDDY -> {
                                babyHandToy.setImageResource(R.drawable.teddy2)
                                BabyItemContent.current_hand_toy = babyItem
                        }
                        BabyItemType.MILK -> {
                        }
                        BabyItemType.NURSERY -> {
                                babyBackground.setImageResource(R.drawable.nursery)
                                BabyItemContent.current_background = babyItem
                        }
                        BabyItemType.PLAYGROUND -> {
                                babyBackground.setImageResource(R.drawable.playground)
                                BabyItemContent.current_background = babyItem
                        }
                        BabyItemType.FOREST -> {
                                babyBackground.setImageResource(R.drawable.forest)
                                BabyItemContent.current_background = babyItem
                        }
                }
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
                val view = LayoutInflater.from(parent.context)
                        .inflate(R.layout.baby_clothes_item, parent, false)

                /*
                setBabyViewByType(BabyItemContent.current_hat)
                setBabyViewByType(BabyItemContent.current_tshirt)
                setBabyViewByType(BabyItemContent.current_pants)
                setBabyViewByType(BabyItemContent.current_hand_toy)
                setBabyViewByType(BabyItemContent.current_background)
                 */
                return ViewHolder(view)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
                val item = values[position]
                holder.imageView.setImageResource(item.imageResource)
                holder.contentView.setOnClickListener {
                        setBabyViewByType(item.type)
                }
        }

        override fun getItemCount(): Int = values.size

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
                val contentView: ConstraintLayout = view.findViewById(R.id.clothesContent)
                val imageView: ImageView = view.findViewById(R.id.clothesImageView)

                override fun toString(): String {
                        return super.toString()
                }

        }



}