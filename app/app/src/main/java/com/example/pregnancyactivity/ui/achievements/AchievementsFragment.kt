package com.example.pregnancyactivity.ui.achievements

import android.content.Context
import android.graphics.ColorMatrix
import android.graphics.ColorMatrixColorFilter
import android.os.Build
import android.os.Bundle
import android.transition.Slide
import android.util.Log
import android.view.*
import android.widget.*
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.pregnancyactivity.R


class AchievementsFragment : Fragment() {

    private lateinit var achievementsViewModel: AchievementsViewModel

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        achievementsViewModel = ViewModelProvider(this).get(AchievementsViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_achievements, container, false)
        //val textView: TextView = root.findViewById(R.id.text_dashboard)
        achievementsViewModel.text.observe(viewLifecycleOwner, Observer {
            //textView.text = it
        })

        val matrix = ColorMatrix()
        matrix.setSaturation(0F) //0 means grayscale
        val cf = ColorMatrixColorFilter(matrix)



        // shared preferences
        val sharedPreferenceAchievements = context?.getSharedPreferences("PREFERENCES_ACHIEVEMENTS", Context.MODE_PRIVATE)
        val sharedPreferenceGeneral = context?.getSharedPreferences("PREFERENCES_GENERAL", Context.MODE_PRIVATE)
        val a1 = sharedPreferenceGeneral!!.getInt("total_AP",0)    //10 total ap
        val a2 = sharedPreferenceAchievements!!.getInt("achievement2",0)      // 4 keer zwemmen
        val a3 = sharedPreferenceAchievements.getInt("achievement3",0)      // 6 keer wandelen
        val a4 = sharedPreferenceAchievements.getInt("achievement4",0)      // 50 dagen app gebruiken
        Log.d("PREF TEST", a4.toString())






        val card1: CardView = root.findViewById(R.id._CardView1)
        val cardImage1 : ImageView = root.findViewById(R.id._imageView1)
        val textView1 : TextView = root.findViewById(R.id._textView1)
        if (a1 < 10) {
            cardImage1.colorFilter = cf
            cardImage1.imageAlpha = 128
            textView1.text = "$a1/10"
        } else {
            textView1.text = "Completed!"
        }
        card1.setOnClickListener{
            val popupWindow: PopupWindow = initPopUp(inflater, "Get 10 activity points in total: $a1/10")
            popupWindow.showAtLocation(this.view, Gravity.CENTER, 0, 0);
        }



        val card2: CardView = root.findViewById(R.id._CardView2)
        val cardImage2 : ImageView = root.findViewById(R.id._imageView2)
        val textView2 : TextView = root.findViewById(R.id._textView2)
        if (a2 < 4) {
            cardImage2.colorFilter = cf
            cardImage2.imageAlpha = 128
            textView2.text = "$a2/4"
        } else {
            textView2.text = "Completed!"
        }
        card2.setOnClickListener{
            val popupWindow: PopupWindow = initPopUp(inflater, "Go swimming 4 times: $a2/4")
            popupWindow.showAtLocation(this.view, Gravity.CENTER, 0, 0);
        }

        val card3: CardView = root.findViewById(R.id._CardView3)
        val cardImage3 : ImageView = root.findViewById(R.id._imageView3)
        val textView3 : TextView = root.findViewById(R.id._textView3)
        if (a3 < 6) {
            cardImage3.colorFilter = cf
            cardImage3.imageAlpha = 128
            textView3.text = "$a3/6"
        } else {
            textView3.text = "Completed!"
        }
        card3.setOnClickListener{
            val popupWindow: PopupWindow = initPopUp(inflater, "Walk 6 times: $a3/6")
            popupWindow.showAtLocation(this.view, Gravity.CENTER, 0, 0);
        }

        val card4: CardView = root.findViewById(R.id._CardView4)
        val cardImage4 : ImageView = root.findViewById(R.id._imageView4)
        val textView4 : TextView = root.findViewById(R.id._textView4)
        var procent : Int = (a4.toFloat()/50f * 100).toInt()
        if (a4 < 50) {
            cardImage4.colorFilter = cf
            cardImage4.imageAlpha = 128

            textView4.text = "$procent%"
        } else {
            textView4.text = "Completed!"
        }

//        cardImage4.clearColorFilter()
//        cardImage4.imageAlpha = 255
        card4.setOnClickListener{
            val popupWindow: PopupWindow = initPopUp(inflater, "Use the app for 50 days: $a4/50")
            popupWindow.showAtLocation(this.view, Gravity.CENTER, 0, 0);
        }




        return root
    }

    private fun initPopUp(inflater: LayoutInflater, text: String) : PopupWindow{
        // Inflate a custom view using layout inflater
        val popUpView = inflater.inflate(R.layout.achievements_pop_up,null)

        // Initialize a new instance of popup window
        val popupWindow = PopupWindow(
                popUpView, // Custom view to show in popup window
                LinearLayout.LayoutParams.MATCH_PARENT, // Width of popup window  wrap: LinearLayout.LayoutParams.WRAP_CONTENT
                200, // Window height
                true    // nodig zodat ge er langs kunt klikken zodat het weg gaat
        )

        // Set an elevation for the popup window
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            popupWindow.elevation = 10.0F
        }


        // If API level 23 or higher then execute the code
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            // Create a new slide animation for popup window enter transition
            val slideIn = Slide()
            slideIn.slideEdge = Gravity.LEFT
            popupWindow.enterTransition = slideIn

            // Slide animation for popup window exit transition
            val slideOut = Slide()
            slideOut.slideEdge = Gravity.RIGHT
            popupWindow.exitTransition = slideOut

        }

        // dismiss the popup window when touched
        popUpView.setOnTouchListener { v, event ->
            popupWindow.dismiss()
            true
        }

        popUpView.findViewById<TextView>(R.id.achievement_text).text = text
        return popupWindow
    }
}