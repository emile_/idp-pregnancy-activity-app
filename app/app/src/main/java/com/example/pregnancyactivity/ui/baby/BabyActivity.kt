package com.example.pregnancyactivity.ui.baby

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ProgressBar
import com.example.pregnancyactivity.R
import com.example.pregnancyactivity.ui.baby.ui.baby.BabyFragment
import com.example.pregnancyactivity.ui.baby.ui.baby.BabyItemContent
import com.google.android.material.bottomnavigation.BottomNavigationView

class BabyActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.baby_activity)


        // calling the action bar
        var actionBar = getSupportActionBar()

        // showing the back button in action bar
        if (actionBar != null) {
            actionBar.title = "My baby"
            actionBar.setDisplayHomeAsUpEnabled(true)
        }

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, BabyFragment.newInstance())
                .commitNow()
        }
        val navView: BottomNavigationView = findViewById(R.id.babyNavView)

        // val navController = findNavController(R.id.babyHostFragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        // val appBarConfiguration = AppBarConfiguration(setOf(
                // R.id.baby, R.id.navigation_achievements, R.id.navigation_friends, R.id.navigation_menu))
        // setupActionBarWithNavController(navController, appBarConfiguration)
        // navView.setupWithNavController(navController)
        navView.setOnNavigationItemSelectedListener {item ->
            when(item.itemId){
                R.id.baby_navigation_clothes ->{
                    BabyItemContent.setClothesItems()
                }
                R.id.baby_navigation_toys ->{
                    BabyItemContent.setToysItems(this)
                }
                R.id.baby_navigation_food ->{
                    BabyItemContent.setFoodItems()
                }
                R.id.baby_navigation_wallpapers ->{
                    BabyItemContent.setBackgroundItems()
                }
                R.id.baby_navigation_settings ->{

                }
                else ->{

                }

            }
            supportFragmentManager.beginTransaction().replace(R.id.container, BabyFragment.newInstance()).commitNow();
            true
        }
        navView.setOnClickListener{


        }
    }
    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }
}