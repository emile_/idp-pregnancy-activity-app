package com.example.pregnancyactivity.ui.mainMenu

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.DividerItemDecoration
import com.example.pregnancyactivity.R
import com.example.pregnancyactivity.ui.activities.menuItem.MenuItemContent
import com.example.pregnancyactivity.ui.baby.ui.baby.BabyFragment


/**
 * A fragment representing a list of Items.
 */
class ActivityItemFragment : Fragment() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_activity_menu_item_list, container, false)

        // Set the adapter
        if (view is RecyclerView) {
            with(view) {
                layoutManager = LinearLayoutManager(context)
                adapter = ActivityItemRecyclerViewAdapter(MenuItemContent.ITEMS, requireActivity())
                view.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
            }
        }
        return view
    }
    companion object {
        fun newInstance() = ActivityItemFragment()
    }

}