package com.example.pregnancyactivity.ui.baby.ui.baby

import android.content.Context
import com.example.pregnancyactivity.R
import java.util.ArrayList
import java.util.HashMap

object BabyItemContent {
    val ITEMS: MutableList<BabyItem> = ArrayList()
    val ITEM_MAP: MutableMap<String, BabyItem> = HashMap()
    var current_hat: BabyItemType = BabyItemType.NONE
    var current_tshirt: BabyItemType = BabyItemType.NONE
    var current_pants: BabyItemType = BabyItemType.NONE
    var current_hand_toy: BabyItemType = BabyItemType.NONE
    var current_background: BabyItemType = BabyItemType.NONE
    var current_hunger: Int = 40

    init {
        setClothesItems()
    }

    fun setClothesItems() {
        ITEMS.clear()
        ITEM_MAP.clear()
        addItem(createBabyItem("Blue t-shirt", R.drawable.tblue, BabyItemType.BLUE_TSHIRT))
        addItem(createBabyItem("Red t-shirt", R.drawable.tred, BabyItemType.RED_TSHIRT))
        addItem(createBabyItem("Purple t-shirt", R.drawable.tpurple, BabyItemType.PURPLE_TSHIRT))
        addItem(createBabyItem("Green pants", R.drawable.pgreen, BabyItemType.GREEN_PANTS))
        addItem(createBabyItem("Yellow pants", R.drawable.pyellow, BabyItemType.YELLOW_PANTS))
        addItem(createBabyItem("Cyan pants", R.drawable.pcyan, BabyItemType.CYAN_PANTS))
        addItem(createBabyItem("Grey cap", R.drawable.hgrey, BabyItemType.GREY_CAP))
        addItem(createBabyItem("Orange cap", R.drawable.horange, BabyItemType.ORANGE_CAP))

    }

    fun setToysItems(context: Context) {
        ITEMS.clear()
        ITEM_MAP.clear()

        val sharedPreference= context.getSharedPreferences("PREFERENCES_GENERAL", Context.MODE_PRIVATE)
        val a1 = sharedPreference!!.getInt("total_AP", 0)
        if (a1 >= 10) {
            addItem(createBabyItem("Rattle", R.drawable.rattle, BabyItemType.RATTLE))
        }
        addItem(createBabyItem("Spoon", R.drawable.spoon, BabyItemType.SPOON))
        addItem(createBabyItem("Teddy", R.drawable.teddy2, BabyItemType.TEDDY))
    }

    fun setFoodItems() {
        ITEMS.clear()
        ITEM_MAP.clear()

        addItem(createBabyItem("Milk", R.drawable.milk, BabyItemType.MILK))
        addItem(createBabyItem("Rice", R.drawable.milk, BabyItemType.MILK))
        addItem(createBabyItem("Chicken", R.drawable.milk, BabyItemType.MILK))
        addItem(createBabyItem("Spaghetti", R.drawable.milk, BabyItemType.MILK))
        addItem(createBabyItem("Ground beef", R.drawable.milk, BabyItemType.MILK))
        addItem(createBabyItem("Eggs", R.drawable.milk, BabyItemType.MILK))
    }

    fun setBackgroundItems() {
        ITEMS.clear()
        ITEM_MAP.clear()

        addItem(createBabyItem("Nursery", R.drawable.nursery, BabyItemType.NURSERY))
        addItem(createBabyItem("Playground", R.drawable.playground, BabyItemType.PLAYGROUND))
        addItem(createBabyItem("Forest", R.drawable.forest, BabyItemType.FOREST))
    }

    private fun addItem(item: BabyItem) {
        ITEMS.add(item)
        ITEM_MAP.put(item.name, item)
    }

    private fun createBabyItem(name: String, imageResource: Int, type: BabyItemType): BabyItem {
        return BabyItem(name, imageResource, type)
    }

    data class BabyItem(val name: String, val imageResource: Int, val type: BabyItemType) {
        override fun toString(): String = name
    }
}
