package com.example.pregnancyactivity.ui.activities.menuItem

import android.media.Image
import com.example.pregnancyactivity.R
import com.example.pregnancyactivity.ui.activities.ScrollingActivity
import com.example.pregnancyactivity.ui.baby.BabyActivity
import java.util.ArrayList
import java.util.HashMap

object MenuItemContent {

    val ITEMS: MutableList<MenuItem> = ArrayList()

    /**
     * A map of sample (dummy) items, by ID.
     */
    val ITEM_MAP: MutableMap<String, MenuItem> = HashMap()


    init {
        // Add some sample items.
        // addItem(createMenuItem("My baby", R.drawable.baby, BabyActivity::class.java))
        addItem(createMenuItem("Running", R.drawable.jogging_blur, ScrollingActivity::class.java))
        addItem(createMenuItem("Swimming", R.drawable.swimming_blur, ScrollingActivity::class.java))
        addItem(createMenuItem("Walking", R.drawable.walking_blur, ScrollingActivity::class.java))
        addItem(createMenuItem("Dancing", R.drawable.dancing_blur, ScrollingActivity::class.java))
        addItem(createMenuItem("Lifting", R.drawable.lifting_blur, ScrollingActivity::class.java))
        addItem(createMenuItem("Cycling", R.drawable.cycling_blur, ScrollingActivity::class.java))
        addItem(createMenuItem("Stair climbing", R.drawable.stairclimb_blur, ScrollingActivity::class.java))
    }

    private fun addItem(item: MenuItem) {
        MenuItemContent.ITEMS.add(item)
        ITEM_MAP.put(item.name, item)
    }

    private fun createMenuItem(name: String, imageResource: Int, nextActivity: Class<*>): MenuItem {
        return MenuItem(name, imageResource, nextActivity)
    }

    /**
     * A dummy item representing a piece of content.
     */
    data class MenuItem(val name: String, val imageResource: Int, val nextActivity: Class<*>) {
        override fun toString(): String = name
    }

}