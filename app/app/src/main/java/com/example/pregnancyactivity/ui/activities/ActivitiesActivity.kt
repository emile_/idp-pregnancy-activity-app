package com.example.pregnancyactivity.ui.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.pregnancyactivity.R
import com.example.pregnancyactivity.ui.mainMenu.ActivityItemFragment

class ActivitiesActivity: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activities_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.container, ActivityItemFragment.newInstance())
                    .commitNow()
        }
        // calling the action bar
        var actionBar = getSupportActionBar()
        // showing the back button in action bar
        if (actionBar != null) {
            actionBar.title = "Activities"
            actionBar.setDisplayHomeAsUpEnabled(true)
        }

    }
    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }
}