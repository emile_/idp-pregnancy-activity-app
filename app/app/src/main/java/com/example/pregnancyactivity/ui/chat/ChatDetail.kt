package com.example.pregnancyactivity.ui.chat

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.pregnancyactivity.R

class ChatDetail : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat_detail)
        // calling the action bar
        var actionBar = getSupportActionBar()

        // showing the back button in action bar
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true)
        }
    }
    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }
}