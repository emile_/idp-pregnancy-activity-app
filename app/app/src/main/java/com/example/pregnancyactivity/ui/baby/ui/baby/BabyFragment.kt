package com.example.pregnancyactivity.ui.baby.ui.baby

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.pregnancyactivity.R


class BabyFragment : Fragment() {

    companion object {
        fun newInstance() = BabyFragment()
    }

    private lateinit var viewModel: BabyViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = inflater.inflate(R.layout.baby_fragment, container, false)
        val hungerBar: ProgressBar = view.findViewById(R.id.hungerBar)
        hungerBar.progress = BabyItemContent.current_hunger
        val rView = view.findViewById<RecyclerView>(R.id.babyRecyclerView)
        if (rView is RecyclerView) {
            with(rView) {
                val lmm = LinearLayoutManager(context)
                lmm.orientation = LinearLayoutManager.HORIZONTAL
                layoutManager = lmm
                adapter = BabyItemAdapter(BabyItemContent.ITEMS,
                        view.findViewById(R.id.hungerBar),
                        view.findViewById<ImageView>(R.id.babyHatView), view.findViewById(R.id.babyTshirtView),
                        view.findViewById(R.id.babyPantsView),
                        view.findViewById(R.id.babyHandToyView),
                        view.findViewById(R.id.babyBackgroundView))
            }
        }
        return view
    }

    /*
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(BabyViewModel::class.java)
        // TODO: Use the ViewModel
    }
     */

}