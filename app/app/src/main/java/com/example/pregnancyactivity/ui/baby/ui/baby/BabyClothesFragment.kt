package com.example.pregnancyactivity.ui.baby.ui.baby

import android.media.Image
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.pregnancyactivity.R

class BabyClothesFragment: Fragment() {

        companion object {
            fun newInstance() = BabyClothesFragment()
        }

        override fun onCreateView(
                inflater: LayoutInflater, container: ViewGroup?,
                savedInstanceState: Bundle?
        ): View {
            val view = inflater.inflate(R.layout.baby_clothes_fragment, container, false)
                /*
                if (view is RecyclerView) {
                    with(view) {
                        val lmm = LinearLayoutManager(context)
                        lmm.orientation = LinearLayoutManager.HORIZONTAL
                        layoutManager = lmm
                        val iv = container!!.findViewById<ImageView>(R.id.babyHatView)
                        adapter = BabyItemAdapter(BabyItemContent.ITEMS, iv, container!!.findViewById(R.id.babyTshirtView), container!!.findViewById(R.id.babyPantsView))

                    }
                }
                 */
            return view
        }
}