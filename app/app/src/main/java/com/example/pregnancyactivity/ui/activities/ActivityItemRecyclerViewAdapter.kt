package com.example.pregnancyactivity.ui.mainMenu

import android.annotation.SuppressLint
import android.content.Intent
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.FragmentActivity
import com.example.pregnancyactivity.MainActivity
import com.example.pregnancyactivity.R
import com.example.pregnancyactivity.ui.activities.menuItem.MenuItemContent
import com.example.pregnancyactivity.ui.baby.BabyActivity


/**
 * [RecyclerView.Adapter] that can display a [DummyItem].
 * TODO: Replace the implementation with code for your data type.
 */
class ActivityItemRecyclerViewAdapter(
        private val values: List<MenuItemContent.MenuItem>,
        private val activity: FragmentActivity
) : RecyclerView.Adapter<ActivityItemRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_activity_menu_item, parent, false)
        return ViewHolder(view)
    }

    @SuppressLint("RestrictedApi")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        holder.contentView.text = item.name
        holder.imageView.setImageResource(item.imageResource)
        holder.fullView.setOnClickListener {
            val intent = Intent(activity, item.nextActivity)
            intent.putExtra("image", item.imageResource)
            intent.putExtra("title", item.toString())
            activity.startActivity(intent)
        }
    }

    override fun getItemCount(): Int = values.size

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        //val idView: TextView = view.findViewById(R.id.item_number)
        val fullView: ConstraintLayout = view.findViewById(R.id.itemLayout)
        val contentView: TextView = view.findViewById(R.id.content)
        val imageView: ImageView = view.findViewById(R.id.menuItemImage)

        override fun toString(): String {
            return super.toString() + contentView.text
        }

    }
}