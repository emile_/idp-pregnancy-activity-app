package com.example.pregnancyactivity.ui.chat

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.LinearLayout
import com.example.pregnancyactivity.R

class ChatActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)

        // calling the action bar
        var actionBar = getSupportActionBar()

        // showing the back button in action bar
        if (actionBar != null) {
            actionBar.title = "Chat"
            actionBar.setDisplayHomeAsUpEnabled(true)
        }


        val friend : LinearLayout = findViewById(R.id.chat1)
        val friend2 : LinearLayout = findViewById(R.id.chat2)

        friend.setOnClickListener {
            val intent = Intent(this, ChatDetail::class.java)
            startActivity(intent)
        }
        friend2.setOnClickListener {
            val intent = Intent(this, ChatDetail::class.java)
            startActivity(intent)
        }
    }
    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }
}