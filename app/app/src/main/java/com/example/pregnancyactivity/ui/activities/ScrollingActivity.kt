package com.example.pregnancyactivity.ui.activities

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.transition.Slide
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import com.example.pregnancyactivity.R
import com.example.pregnancyactivity.ui.chat.ChatActivity
import com.example.pregnancyactivity.ui.chat.ChatDetail
import kotlin.math.min

class ScrollingActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scrolling)
        setSupportActionBar(findViewById(R.id.toolbar))
        val title = intent.getStringExtra("title")
        val imageresource = intent.getIntExtra("image", 0)
        //val title = savedInstanceState!!.getString("title")
        val toolbar = findViewById<CollapsingToolbarLayout>(R.id.toolbar_layout)
        toolbar.title = title
        toolbar.setBackgroundResource(imageresource)
        /*findViewById<FloatingActionButton>(R.id.addWorkoutTimeFab).setOnClickListener { view ->
            Snackbar.make(view, "Adding workout time", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
        }
        */
        // calling the action bar
        var actionBar = getSupportActionBar()

        // showing the back button in action bar
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true)
        }

        findViewById<FloatingActionButton>(R.id.addWorkoutTimeFab).setOnClickListener {
            val popupWindow: PopupWindow = initPopUp(layoutInflater, title!!)
            popupWindow.showAtLocation(findViewById(R.id.content_container), Gravity.CENTER, 0, 0);
        }

        findViewById<ImageButton>(R.id.addFriendButton).setOnClickListener {
            val popupWindow: PopupWindow = initAddFriendPopup(layoutInflater, title!!, this)
            popupWindow.showAtLocation(findViewById(R.id.content_container), Gravity.CENTER, 0, 0);
        }

        //findViewById<TextView>(R.id.largeTextview).text = getString(R.id.run)
    }
    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }
    private fun initAddFriendPopup(inflater: LayoutInflater, title: String, activity: Activity) : PopupWindow {
        // Inflate a custom view using layout inflater
        val popUpView = inflater.inflate(R.layout.find_friends_popup,null)
        // Initialize a new instance of popup window
        val popupWindow = PopupWindow(
                popUpView, // Custom view to show in popup window
                LinearLayout.LayoutParams.MATCH_PARENT, // Width of popup window  wrap: LinearLayout.LayoutParams.WRAP_CONTENT
                730, // Window height
                true    // nodig zodat ge er langs kunt klikken zodat het weg gaat
        )
        // Set an elevation for the popup window
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            popupWindow.elevation = 10.0F
        }
        // If API level 23 or higher then execute the code
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            // Create a new slide animation for popup window enter transition
            val slideIn = Slide()
            slideIn.slideEdge = Gravity.LEFT
            popupWindow.enterTransition = slideIn

            // Slide animation for popup window exit transition
            val slideOut = Slide()
            slideOut.slideEdge = Gravity.RIGHT
            popupWindow.exitTransition = slideOut

        }
        // dismiss the popup window when touched
        popUpView.setOnTouchListener { v, event ->
            popupWindow.dismiss()
            true
        }
        popUpView.findViewById<TextView>(R.id.findBuddiesTitle).text = "Find "+title.toLowerCase()+" buddies"
        popUpView.findViewById<LinearLayout>(R.id.person1).setOnClickListener{
            val intent = Intent(activity, ChatDetail::class.java)
            // intent.putExtra("image", item.imageResource)
            // intent.putExtra("title", item.toString())
            activity.startActivity(intent)
        }
        popUpView.findViewById<LinearLayout>(R.id.person2).setOnClickListener{
            val intent = Intent(activity, ChatDetail::class.java)
            activity.startActivity(intent)
        }
        popUpView.findViewById<LinearLayout>(R.id.person3).setOnClickListener{
            val intent = Intent(activity, ChatDetail::class.java)
            activity.startActivity(intent)
        }

        return popupWindow
    }

    private fun initPopUp(inflater: LayoutInflater, title: String) : PopupWindow {
        // Inflate a custom view using layout inflater
        val popUpView = inflater.inflate(R.layout.workout_time_popup,null)
        popUpView.findViewById<Spinner>(R.id.intensitySpinner).setSelection(1)

        // Initialize a new instance of popup window
        val popupWindow = PopupWindow(
                popUpView, // Custom view to show in popup window
                LinearLayout.LayoutParams.MATCH_PARENT, // Width of popup window  wrap: LinearLayout.LayoutParams.WRAP_CONTENT
                300, // Window height
                true    // nodig zodat ge er langs kunt klikken zodat het weg gaat
        )

        // Set an elevation for the popup window
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            popupWindow.elevation = 10.0F
        }


        // If API level 23 or higher then execute the code
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            // Create a new slide animation for popup window enter transition
            val slideIn = Slide()
            slideIn.slideEdge = Gravity.LEFT
            popupWindow.enterTransition = slideIn

            // Slide animation for popup window exit transition
            val slideOut = Slide()
            slideOut.slideEdge = Gravity.RIGHT
            popupWindow.exitTransition = slideOut

        }

        // dismiss the popup window when touched
        popUpView.setOnTouchListener { v, event ->
            popupWindow.dismiss()
            true
        }
        popUpView.findViewById<ImageButton>(R.id.confirmWorkoutTimeButton).setOnClickListener{
            val time = popUpView.findViewById<EditText>(R.id.editTextNumber)
            val minutes = time.text.toString().toInt()
            val spinner = popUpView.findViewById<Spinner>(R.id.intensitySpinner)
            var intesity : Float = 0f
            if (spinner.selectedItemPosition == 0) {
                intesity = 0.5f
            }
            if (spinner.selectedItemPosition == 1) {
                intesity = 1f
            }
            if (spinner.selectedItemPosition == 2) {
                intesity = 1.5f
            }

            //shared preferences algemene data
            val sharedPreferenceGeneral = getSharedPreferences("PREFERENCES_GENERAL", Context.MODE_PRIVATE)
            val editorGeneral = sharedPreferenceGeneral.edit()
            var workoutAP : Int = calculateAP(minutes, intesity,  title).toInt()

            var totalAP = sharedPreferenceGeneral.getInt("total_AP", 0)
            totalAP += workoutAP
            editorGeneral.putInt("total_AP", totalAP)
            var dailyAP = sharedPreferenceGeneral.getInt("daily_AP", 0)
            dailyAP += workoutAP
            editorGeneral.putInt("daily_AP", dailyAP)

            var workouts = sharedPreferenceGeneral.getString("workouts", "")
            workouts = workouts + "$title: $minutes minutes, $intesity intensity, $workoutAP AP earned.\n"
            editorGeneral.putString("workouts", workouts)

            editorGeneral.apply()
            Log.d("ACTIVITY", "add workout time")
            Log.d("ACTIVITY", "$title, $minutes, $intesity = $workoutAP")
            popupWindow.dismiss()
            true
        }

        return popupWindow
    }

    fun calculateAP(minutes : Int, intesity : Float, activity : String) : Float {
        var MET = 1f
        if (activity == "Running")
            MET = 6f
        if (activity == "Swimming")
            MET = 8f
        if (activity == "Walking")
            MET = 3.5f
        if (activity == "Dancing")
            MET = 8f
        if (activity == "Lifting")
            MET = 6f
        if (activity == "Cycling")
            MET = 5f

        val AP = MET * minutes.toFloat() / 5f * intesity
        return AP
    }
}